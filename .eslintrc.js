module.exports = {
    extends: [
        'next/core-web-vitals',
    ],
    rules: {
        'no-unused-vars': process.env.NODE_ENV === 'production' ? 'off' : 'warn',
        'react/no-unescaped-entities': 'off',
    },
};

import React from "react";
import { CircularProgress, makeStyles } from "@mui/material";

// const useStyles = makeStyles((theme) => ({
//     root: {
//         display: "flex",
//         alignItems: "center",
//         justifyContent: "center",
//         height: "100vh",
//         backgroundColor: theme.palette.background.default,
//     },
// }));

const Loading = () => {

    //  const classes = useStyles(); className={classes.root}

    return (
        <div style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "100vh",
            // backgroundColor: theme.palette.background.default,
        }}>
            <CircularProgress />
        </div>
    );

}

export default Loading;
// app/dashboard/page.tsx
import React from "react";
import { Container, Grid, Typography } from "@mui/material";
import LeaderBoard from "@/components/leader-board";
import UpcomingMatches from "@/components/upcoming-matches";
import UserBets from "@/components/user-bets";
// import LoginButton from "@/components/login-button";
// import FirestoreTest from "@/components/firestore-test";

const DashboardPage = () => {
    return (
        <Container sx={{ mt: 10 }}>
            <Grid container spacing={4}>
                <Grid item xs={12} md={6}>
                    <UpcomingMatches />
                </Grid>
                <Grid item xs={12} md={6}>
                    <LeaderBoard />
                </Grid>
                <Grid item xs={12}>
                    <UserBets />
                </Grid>
                {/*<Grid item xs={12}>
                    <FirestoreTest />
                </Grid> */}
            </Grid>
        </Container>
    );
};

export default DashboardPage;

"use client"

import Header from "@/components/header";
import { useIsAuthenticated } from "@azure/msal-react";
import { useEffect } from "react";
import { useRouter } from 'next/navigation';
import Loading from "./loading";

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode
}) {

  const isAuthenticated = useIsAuthenticated();
  const router = useRouter();

  useEffect(() => {
    if (!isAuthenticated) {
      router.replace('/');
    }
  }, [isAuthenticated]);

  return (
    <>
      {
        isAuthenticated && (
          <div className="dashboard" style={{ minHeight: '100vh' }}>
            <Header />
            <main>{children}</main>
          </div>
        )
      }
      {!isAuthenticated && (
        <Loading />
      )}
    </>
  );
}
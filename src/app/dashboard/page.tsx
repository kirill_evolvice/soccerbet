'use client'

import { usePathname } from 'next/navigation'
import DashboardPage from "./DashboardPage";

export default function Dashboard() {
  const router = usePathname();

  return (
    <slot name="content">
      {router === '/dashboard' &&
        <>
          <DashboardPage />
        </>
      }
    </slot>
  );
}

import Image from "next/image";
import { Grid, Box } from "@mui/material";
import styles from "./page.module.css";
import LoginButton from "@/components/login-button";

export default function Home() {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', minHeight: '100vh', padding: '3px' }}>
      <Grid container spacing={2} justifyContent="center" alignItems="center">
        <Grid item className={styles.center}>
          <Image
            className={styles.logo}
            src="/logoDark.png.webp"
            alt="Evolvice logo"
            width={264}
            height={26}
            priority
          />
        </Grid>
        <Grid item>
          <Image
            src="/euro-icon.png"
            alt="Evolvice logo"
            width={500}
            height={500}
            priority
          />
        </Grid>
      </Grid>
      <Box sx={{ marginTop: 2 }}>
        <LoginButton />
      </Box>
    </Box>
  );
}

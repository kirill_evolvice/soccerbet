// firebaseConfig.ts
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDMQUDjqgUQ9STLNv9Rwn-IJVuFzwsTw84",
  authDomain: "soccer-bet-9b90f.firebaseapp.com",
  projectId: "soccer-bet-9b90f",
  storageBucket: "soccer-bet-9b90f.appspot.com",
  messagingSenderId: "806707650176",
  appId: "1:806707650176:web:ca1396ea316d6fe4675065"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firestore
const db = getFirestore(app);

export { db };

// components/LoginButton.tsx
"use client";

import { useMsal } from "@azure/msal-react";
import { loginRequest } from "@/msal-config";
import { Button } from '@mui/material';
import { useIsAuthenticated } from "@azure/msal-react";
import { useRouter } from 'next/navigation'
import { useEffect } from "react";
import MicrosoftIcon from '@mui/icons-material/Microsoft';

const LoginButton = () => {
    const { instance } = useMsal();
    const isAuthenticated = useIsAuthenticated();
    const router = useRouter();

    const handleLogin = () => {
        instance.loginRedirect(loginRequest).catch(e => {
            console.error(e);
        });
    };

    useEffect(() => {
        if (isAuthenticated) {
            router.replace('/dashboard');
        }
    }, [isAuthenticated]);

    return (
        <>
            {!isAuthenticated && (
                <>
                    <Button variant="contained" color="primary" onClick={handleLogin} startIcon={<MicrosoftIcon />}>
                        Login with Microsoft
                    </Button>
                </>
            )}
        </>
    );
};

export default LoginButton;

"use client"

import React from "react";
import styles from "./header.module.css";
import { AppBar, Toolbar, Typography, IconButton, Box } from "@mui/material";
import LogoutIcon from "@mui/icons-material/Logout";
import Image from "next/image";
import UserProfile from "../user-profile";
import { useMsal } from "@azure/msal-react";

const Header: React.FC = () => {
    const { instance } = useMsal();

    const handleSignOut = () => {
        instance.logoutRedirect().catch((error) => {
            console.error("Logout failed:", error);
        });
    };

    return (
        <AppBar position="static" color="transparent" elevation={0}>
            <Toolbar sx={{ justifyContent: "space-between" }}>
                {/* User Name on the left */}
                <UserProfile />

                {/* Images in the center */}
                <Box sx={{ display: "flex", alignItems: "center" }} className={styles.center}>
                    <Image src="/logoDark.png.webp" alt="Evolvice logo" width={132} height={13} priority className={styles.logo} />
                    <Image src="/euro-icon.png" alt="Evolvice logo" width={80} height={80} priority />
                </Box>

                {/* Sign out on the right */}
                <Box sx={{ display: "flex", alignItems: "center" }}>
                    <Typography variant="body1" sx={{ marginRight: 1, cursor: "pointer" }} onClick={handleSignOut}>
                        Sign Out
                    </Typography>
                    <IconButton color="inherit" onClick={handleSignOut}>
                        <LogoutIcon />
                    </IconButton>
                </Box>
            </Toolbar>
        </AppBar>
    );
};

export default Header;

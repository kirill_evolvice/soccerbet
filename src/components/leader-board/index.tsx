// components/LeaderBoard.tsx
import React from "react";
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography, Link } from "@mui/material";

const leaderboard = [
    { id: 1, name: "User A", points: 100 },
    { id: 2, name: "User B", points: 90 },
    { id: 3, name: "User C", points: 80 },
    { id: 4, name: "User D", points: 70 },
    { id: 5, name: "User E", points: 60 },
    // Add more users here
];

const LeaderBoard = () => {
    return (
        <div>
            <Typography variant="h5" gutterBottom>
                Leaderboard - #10
            </Typography>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="left"><strong>Rank</strong></TableCell>
                            <TableCell align="left"><strong>Name</strong></TableCell>
                            <TableCell align="left"><strong>Points</strong></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {leaderboard.map((user, index) => (
                            <TableRow key={user.id}>
                                <TableCell align="left">{index + 1}</TableCell>
                                <TableCell align="left">{user.name}</TableCell>
                                <TableCell align="left">{user.points}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default LeaderBoard;

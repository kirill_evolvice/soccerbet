// components/MsalProviderWrapper.tsx
"use client";

import { ReactNode } from "react";
import { MsalProvider } from "@azure/msal-react";
import { PublicClientApplication } from "@azure/msal-browser";
import { msalConfig } from "@/msal-config";

const msalInstance = new PublicClientApplication(msalConfig);

interface MsalProviderWrapperProps {
  children: ReactNode;
}

const MsalProviderWrapper = ({ children }: MsalProviderWrapperProps) => {
  return <MsalProvider instance={msalInstance}>{children}</MsalProvider>;
};

export default MsalProviderWrapper;

import React from "react";
import { Card, CardContent, Typography, List, ListItem, ListItemText } from "@mui/material";

const userBets = [
    { id: 1, match: "Team A vs Team B", bet: "Team A", date: "2024-06-10", winner: "Team A" },
    { id: 2, match: "Team C vs Team D", bet: "Team D", date: "2024-06-12", winner: "Team C" },
    // Add more bets here
];

const UserBets = () => {
    const getTextColor = (bet: string, winner: string) => {
        return bet === winner ? "success" : "error";
    };

    return (
        <div>
            <Typography variant="h5" gutterBottom>
                Score: 10 Points
            </Typography>
            <Card>
                <CardContent>
                    <List>
                        {userBets.map((bet) => (
                            <ListItem key={bet.id}>
                                <ListItemText
                                    primary={`Match: ${bet.match}`}
                                    secondary={`Bet: ${bet.bet} on ${bet.date} - Winner: ${bet.winner}`}
                                    sx={{ color: getTextColor(bet.bet, bet.winner) === "success" ? "green" : "red" }}
                                />
                            </ListItem>
                        ))}
                    </List>
                </CardContent>
            </Card>
        </div>
    );
};

export default UserBets;

// components/SignIn.tsx
"use client";

import { useState, FormEvent } from 'react';
import { TextField, Button, Typography, Box } from '@mui/material';
import { useMsal } from "@azure/msal-react";
import { loginRequest } from '@/msal-config';

interface SignInProps {
  switchToSignUp: () => void;
}

const SignIn = ({ switchToSignUp }: SignInProps) => {
  const { instance } = useMsal();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    instance.loginPopup(loginRequest)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.error(error);
      });
  };

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    handleLogin();
  };

  return (
    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
      <Typography variant="h5" component="h1">
        Sign In
      </Typography>
      <TextField
        margin="normal"
        required
        fullWidth
        label="Email Address"
        autoComplete="email"
        autoFocus
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <TextField
        margin="normal"
        required
        fullWidth
        label="Password"
        type="password"
        autoComplete="current-password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
        Sign In
      </Button>
      <Button fullWidth variant="outlined" onClick={switchToSignUp}>
        Don&apos;t have an account? Sign Up
      </Button>
    </Box>
  );
};

export default SignIn;

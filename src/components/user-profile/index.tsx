"use client"

import { useState, useEffect } from "react";
import { useMsal } from "@azure/msal-react";
import { loginRequest } from "@/msal-config";
import { InteractionRequiredAuthError } from "@azure/msal-browser";
import { Avatar, Typography, CircularProgress } from "@mui/material";

const UserProfile = () => {
    const { instance, accounts } = useMsal();
    const [userData, setUserData] = useState<any>(null);
    const [userPhoto, setUserPhoto] = useState<string | null>(null);

    useEffect(() => {
        if (accounts.length > 0) {
            const request = {
                ...loginRequest,
                account: accounts[0],
            };

            instance.acquireTokenSilent(request).then((response) => {
                fetchUserData(response.accessToken);
                fetchUserPhoto(response.accessToken);
            }).catch((error) => {
                if (error instanceof InteractionRequiredAuthError) {
                    instance.acquireTokenRedirect(request);
                } else {
                    console.error(error);
                }
            });
        }
    }, [accounts, instance]);

    const fetchUserData = async (accessToken: string) => {
        const response = await fetch("https://graph.microsoft.com/v1.0/me", {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        });
        const data = await response.json();
        setUserData(data);
    };

    const fetchUserPhoto = async (accessToken: string) => {
        const response = await fetch("https://graph.microsoft.com/v1.0/me/photo/$value", {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        });

        if (response.ok) {
            const blob = await response.blob();
            const reader = new FileReader();
            reader.onloadend = () => {
                setUserPhoto(reader.result as string);
            };
            reader.readAsDataURL(blob);
        } else {
            console.error("Error fetching user photo", response.statusText);
        }
    };

    if (!userData) {
        return <CircularProgress />;
    }

    return (
        <div style={{ display: "flex", alignItems: "center" }}>
            {userPhoto && <Avatar src={userPhoto} alt="User Photo" sx={{ width: 50, height: 50, marginRight: 2 }} />}
            <div>
                <Typography variant="h6">{userData.displayName}</Typography>
            </div>
        </div>
    );
};

export default UserProfile;

// components/SignUp.tsx
import { useState, FormEvent } from 'react';
import { TextField, Button, Typography, Box } from '@mui/material';

interface SignUpProps {
  switchToSignIn: () => void;
}

const SignUp = ({ switchToSignIn }: SignUpProps) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    // Handle sign-up logic here
  };

  return (
    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1, maxWidth: '400px', p: 2 }}>
      <Typography variant="h5" component="h1">
        Sign Up
      </Typography>
      <TextField
        margin="normal"
        required
        fullWidth
        label="Email Address"
        autoComplete="email"
        autoFocus
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <TextField
        margin="normal"
        required
        fullWidth
        label="Password"
        type="password"
        autoComplete="new-password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <TextField
        margin="normal"
        required
        fullWidth
        label="Confirm Password"
        type="password"
        value={confirmPassword}
        onChange={(e) => setConfirmPassword(e.target.value)}
      />
      <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
        Sign Up
      </Button>
      <Button fullWidth variant="outlined" onClick={switchToSignIn}>
        Already have an account? Sign In
      </Button>
    </Box>
  );
};

export default SignUp;

import React, { useEffect, useState } from "react";
import { Card, CardContent, Typography, Button, Grid } from "@mui/material";
import SportsSoccerIcon from '@mui/icons-material/SportsSoccer';
import Flag from "react-world-flags";
import {countryNameToCode} from "../../utlis/helpers";
import styles from "./upcomingMatches.module.css";
import BetModal from "../betmodal";

interface Match {
    id: number;
    teamA: string;
    teamB: string;
    date: string;
}

const upcomingMatches = [
    { id: 1, teamA: "Georgia", teamB: "Ukraine", date: "2024-06-10" },
    { id: 2, teamA: "Italy", teamB: "Germany", date: "2024-06-11" },
    // Add more matches here
];

const UpcomingMatches = () => {
    const [matches, setMatches] = useState<Match[]>([]);
    const [selectedMatch, setSelectedMatch] = useState<Match | null>(null);
    const [isBetModalOpen, setIsBetModalOpen] = useState(false);

    // const handleBet = (matchId: number) => {
    //     console.log("Bet placed on match:", matchId);
    //     // Implement bet logic here
    // };

    // useEffect(() => {
    //     // Fetch matches from the backend
    //     fetch("/api/matches") // Replace with your actual API endpoint
    //         .then(response => response.json())
    //         .then(data => setMatches(data))
    //         .catch(error => console.error("Error fetching matches:", error));
    // }, []);

    const handleBetButtonClick = (match: Match) => {
        setSelectedMatch(match);
        setIsBetModalOpen(true);
    };

    const handleCloseBetModal = () => {
        setIsBetModalOpen(false);
        setSelectedMatch(null);
    };

    return (
        <div>
            <Typography variant="h5" gutterBottom>
                Upcoming Matches
            </Typography>
            <Grid container spacing={2}>
                {upcomingMatches.map((match) => (
                    <Grid item xs={12} key={match.id}>
                        <Card sx={{ display: 'flex', alignItems: 'center', padding: 2 }}>
                            <Grid container spacing={2} alignItems="center">
                                <Grid item xs={8}>
                                <Typography variant="h6">
                                    <div className={styles['flags-container']} >
                                      <Flag code={countryNameToCode(match.teamA)} alt={`${match.teamA} flag`}  />
                                    </div>
                                   
                                    {match.teamA}
                                   
                                   <span style={{ margin: "0 10px" }}>vs</span>  
                                    <div className={styles['flags-container']}>
                                      <Flag code={countryNameToCode(match.teamB)} alt={`${match.teamB} flag`}  />
                                    </div>
                                    {match.teamB}
                                    
                                </Typography>
                                <Typography color="textSecondary">{match.date}</Typography>
                                </Grid>
                                <Grid item xs={4} container justifyContent="flex-end">
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        endIcon={<SportsSoccerIcon />}
                                        onClick={() => handleBetButtonClick(match)}
                                    >
                                        Bet
                                    </Button>
                                </Grid>
                            </Grid>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            {selectedMatch && (
                <BetModal
                    match={selectedMatch}
                    open={isBetModalOpen}
                    onClose={handleCloseBetModal}
                />
            )}
        </div>
    );
};

export default UpcomingMatches;

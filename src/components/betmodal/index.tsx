import React, { useState } from "react";
import { Modal, Box, Typography, Button, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel } from "@mui/material";

interface BetModalProps {
    open: boolean;
    onClose: () => void;
    match: {
        id: number;
        teamA: string;
        teamB: string;
        date: string;
    };
}

const BetModal: React.FC<BetModalProps> = ({ open, onClose, match }) => {
    const [selectedTeam, setSelectedTeam] = useState<string>("");

    const handleBetSubmit = () => {
        console.log("Bet placed on:", selectedTeam);
        // Implement bet submission logic here
        onClose();
    };

    const handleTeamChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedTeam((event.target as HTMLInputElement).value);
    };

    return (
        <Modal open={open} onClose={onClose}>
            <Box
                sx={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    width: 400,
                    bgcolor: "background.paper",
                    border: "2px solid #000",
                    boxShadow: 24,
                    p: 4,
                }}
            >
                <Typography variant="h6" component="h2">
                    Place Your Bet
                </Typography>
                <Typography sx={{ mt: 2 }}>
                    {match.teamA} vs {match.teamB}
                </Typography>
                <Typography sx={{ mt: 2 }}>
                    Date: {match.date}
                </Typography>
                <FormControl component="fieldset" sx={{ mt: 2 }}>
                    <FormLabel component="legend">Choose the winning team</FormLabel>
                    <RadioGroup
                        aria-label="team"
                        name="team"
                        value={selectedTeam}
                        onChange={handleTeamChange}
                    >
                        <FormControlLabel value={match.teamA} control={<Radio />} label={match.teamA} />
                        <FormControlLabel value={match.teamB} control={<Radio />} label={match.teamB} />
                    </RadioGroup>
                </FormControl>
                <div>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleBetSubmit}
                        disabled={!selectedTeam}
                        sx={{ mt: 2 }}
                    >
                        Place Bet
                    </Button>
                </div>
                
            </Box>
        </Modal>
    );
};

export default BetModal;

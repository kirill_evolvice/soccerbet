// components/FirestoreTest.tsx
"use client"

import React, { useEffect, useState } from "react";
import { db } from "@/firebase-config";
import { collection, onSnapshot } from "firebase/firestore";

const FirestoreTest = () => {
    const [data, setData] = useState<any[]>([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const unsubscribe = onSnapshot(collection(db, "users"), (snapshot) => {
            const docsData = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
            setData(docsData);
            setLoading(false);
        }, (error) => {
            console.error("Error fetching data: ", error);
            setLoading(false);
        });

        // Cleanup listener on component unmount
        return () => unsubscribe();
    }, []);

    if (loading) {
        return <p>Loading...</p>;
    }

    return (
        <div>
            <h3>Firestore Data:</h3>
            <pre>{JSON.stringify(data, null, 2)}</pre>
        </div>
    );
};

export default FirestoreTest;
